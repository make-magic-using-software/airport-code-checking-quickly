# Airport Code Checking Quickly

## Background
My wife works at the Qatar Airways in Chongqing, China. And she is using a booking ticket system that is not smart. So I develop the tool for her that she can quickly search the airport shorthand.

## Problem
The customer service is calling with a client who needs to book a ticket. The customer service uses Amadeus booking system.  The customer service has to check airport code by Amadeus. But the codes list show all airport codes including no business airport. It makes customer service has low inefficient. This information comes from my wife because she just joined Qatar Airways as a phone customer service and have to memorize airport code. And I'd like to solve it.

## Solution
1. Develop a Chrome extension.
2. Customize bind form.
3. Only searching for the airport that has a business.
4. Support airport code, airport name, country, and city by blur searching.
5. Use third searching json libs to support search.
6. Mark the English word of city pronunciation.  
7. Switch language Chinese and English.

## How to build?
```
npm install
npm run build
```

## Example
### Qatar Airways
1. City List  
`https://www.qatarairways.com/content/Qatar/common/cityLists/citylists/cityList_en.json`
2. Airport Code  
`https://www.qatarairways.com/content/Qatar/common/routes/booking_fromRoutes/from_iatacodes.json`
3. All Destination  
`http://j.mp/2JDDjjt`
![alt Qatar](demo.gif)


## Others
1. [Airline Company](https://www.seatguru.com/browseairlines)

2. [Top 10 Airline Company](https://www.forbes.com/sites/ericrosen/2018/11/16/the-2019-list-of-the-worlds-best-airlines-is-out-now/#4bc8ba44470f)

3. [Airline Ticket Booking System](https://www.capterra.com/airline-reservation-system-software/)

