import { EDIT, CONFIRM } from '@common/config';

// this method is to subscribe the popup's set and confirm events
// and do setting and confirm
export default function() {

    chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
        if (request.action === EDIT) {
            console.log(EDIT);
            sendResponse({farewell: "goodbye"});
        } else if (request.action === CONFIRM) {
            console.log(CONFIRM);
        }
    });

}
