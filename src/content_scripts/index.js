import { ajax } from '@common/utils';
import {EDIT, CONFIRM, API_CONFIG, FUSE_CONFIG } from '@common/config';
const Fuse = require('@common/fuse');
import './style.less';
import setting from './setting';



main();

async function main() {

    // set which input form could auto tip
    setting();

    // default fetch data and bind event
    let cityListStr = await ajax({url: API_CONFIG.qatarAirways.cityList});

    let cityObj = JSON.parse(cityListStr);


    bindAutoSearch(cityObj);
}

// auto
// @list {Array} the airport code list [{city, airport,...}...]
// @return {Undefined}
function bindAutoSearch(list) {

    // "list" is the item array
    var fuse = new Fuse(list, FUSE_CONFIG);

    // input event bind
    let inputsDom = document.querySelectorAll('input');
    inputsDom.forEach((item) => {
        let boundClient = item.getBoundingClientRect();
        let _compedStyle = window.getComputedStyle(item, null);
        let inputOuterHeight = parseInt(_compedStyle.getPropertyValue('height'));
            inputOuterHeight += parseInt(_compedStyle.getPropertyValue('padding-top')) * 2;
        let position = {
            top: boundClient.top + inputOuterHeight + 4,
            left: boundClient.left - 5,
        }

        // avoid clouser
        item.addEventListener('input', (function(_item, _position){
            return function(){
                let val = _item.value.replace(/^\s+|\s+$/g, '');
                var result = fuse.search(_item.value);
                if (val === '') {
                    result = [];
                }
                refreshSuggest(result, _position, _item);
            }
        })(item, position));
    });
}

// render suggest airport code
// @data {Array}
// @inputDom {DOM} the current input dom node
function refreshSuggest(data,_position, _item) {

    let htmlsArr = data.map((item) => {
        return `<tr>
            <td>${item.city}</td>
            <td>${item.airport}</td>
            <td>${item.country}</td>
            <td>${item.shorthand}</td>
        </tr>` ;
    });

    let airportBody = createFreeDom();
    airportBody.innerHTML = htmlsArr.join('');

    let airportRootDom = airportBody.parentElement.parentElement;

    airportRootDom.style.left = _position.left + 'px';
    airportRootDom.style.top = _position.top + 'px';

    document.body.appendChild(airportRootDom);
}



// create a free dom as the airport code container
// using singleton
// @return {DOM}
function createFreeDom() {
    if (createFreeDom.instance) {
        return createFreeDom.instance;
    }
    let airportDom = document.createElement('div');
    airportDom.innerHTML =  `
        <table>
        <thead>
            <tr><th>City</th><th>Airport</th><th>Country</th><th>Shorthand</th></tr>
        </thead>
        <tbody></tbody></table>
    `;
    airportDom.className = "airport_code_checking_quickly";
    createFreeDom.instance = airportDom.querySelector('tbody');
    return  createFreeDom.instance;
}
