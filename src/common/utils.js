
// @opts {Object} ajax config paras
// @return {Promise} the Promise instance
function ajax(opts) {
    return  new Promise(function(resolve, reject){
        var xhr = new XMLHttpRequest();
        xhr.open(opts.method || 'GET', opts.url, true);
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                resolve(xhr.responseText);
            }
        }
        xhr.send();
    });
}

export {ajax};
