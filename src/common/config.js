const EDIT = 'edit';

const CONFIRM = 'confirm';

const API_CONFIG = {

    // qatar airways api
    qatarAirways: {
        cityList: "https://www.qatarairways.com/content/Qatar/common/cityLists/citylists/cityList_en.json",

    }
};

const FUSE_CONFIG = {
    shouldSort: true,
    threshold: 0.27,
    location: 0,
    distance: 2,
    maxPatternLength: 10,
    minMatchCharLength: 2,
    keys: [
        "city",
        "airport",
        "country",
    ]
};

export {EDIT, CONFIRM, API_CONFIG, FUSE_CONFIG};
