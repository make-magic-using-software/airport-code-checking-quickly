import {EDIT, CONFIRM } from '@common/config';

// @obj {Object}
function sendToContentScript(obj){
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        chrome.tabs.sendMessage(tabs[0].id, obj, function(response) {
            // callback
        });
    });
}

var setDom = document.getElementById('set');
var confirmDom = document.getElementById('confirm');


setDom.addEventListener('click', function(){
    sendToContentScript({ action: EDIT });
}, false);

confirmDom.addEventListener('click', function(){
    sendToContentScript({ action: CONFIRM });
}, false);
