const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: {
        content_script: path.resolve(__dirname, './src/content_scripts/index.js'),
        popup: path.resolve(__dirname, './src/popup/index.js'),
    },
    output: {
        filename: './[name].js',
        path: path.resolve(__dirname, 'dist')
    },
    mode: 'development',
    devtool: 'inline-source-map',
    watch: true,
    resolve: {
        alias: {
            "@common": path.resolve(__dirname, './src/common'),
        },
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|common)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: [
                            ["@babel/plugin-transform-runtime",
                                {
                                    "regenerator": true
                                }
                            ]
                        ]
                    }
                }
            },
            {
                test: /\.less$/,
                use: [
                    {
                        loader: 'style-loader', // creates style nodes from JS strings
                    },
                    {
                        loader: 'css-loader', // translates CSS into CommonJS
                    },
                    {
                        loader: 'less-loader', // compiles Less to CSS
                    },
                ],
            },
        ],
    },
    plugins: [
        new CopyPlugin([
            { from: './src/manifest.json', to: './manifest.json' },
            { from: './src/index.html', to: './index.html' },
            { from: './src/popup/index.html', to: './popup.html' },
        ]),
    ],

    //for test
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9000
    },
};
